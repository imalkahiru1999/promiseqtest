import { generateUniqueId } from './generateUniqueId'

describe('generateUniqueId', () => {
  it('should generate a non-empty ID', () => {
    const id = generateUniqueId();
    expect(id).toBeTruthy();
    expect(typeof id).toBe('string');  //check whether the id is emptry or not
  });

  it('should generate a unique ID', () => {
    const id1 = generateUniqueId();
    const id2 = generateUniqueId();
    expect(id1).not.toEqual(id2); // check whether the id is unique or not
  });
});


